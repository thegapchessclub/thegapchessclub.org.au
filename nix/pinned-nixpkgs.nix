let
  fetcher = { rev, sha256, ... }: builtins.fetchTarball {
    inherit sha256;
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  };
in
  import (fetcher (builtins.fromJSON (builtins.readFile ./pinned-nixpkgs.json))) {}
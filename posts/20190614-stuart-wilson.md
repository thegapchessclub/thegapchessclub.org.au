---
title: 2019 Stuart Wilson (14 June)
date: 2019-06-14
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Tony Morris            | 0     | John Nothdurft         | 1     |
| Rex Scarf              | 0     | Andrew Robinson        | 1     |
| Warren Ward            | 1     | Brian Willcock         | 0     |
| George Flitcroft-Smith | 1     | Mark Pendrith          | 0     |

----

##### Tony Morris v John Nothdurft

<iframe src="https://lichess.org/embed/9FhHxN76?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Rex Scarf v Andrew Robinson

<iframe src="https://lichess.org/embed/ognb5OFx?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Warren Ward v Brian Willcock

<iframe src="https://lichess.org/embed/T84b4dBQ?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### George Flitcroft-Smith v Mark Pendrith

<iframe src="https://lichess.org/embed/MXk55IeR?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

---
title: 2019 Stuart Wilson (21 June)
date: 2019-06-21
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Mark Pendrith          | 0     | Tony Morris            | 1     |
| George Flitcroft-Smith | 1     | John Nothdurft         | 0     |
| Warren Ward            | 1     | Martin Laizans         | 0     |
| Brian Willcock         | 1/2   | Andrew Robinson        | 1/2   |

----

##### Mark Pendrith v Tony Morris

<iframe src="https://lichess.org/embed/VJU6zvTP?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### George Flitcroft-Smith v John Nothdurft

<iframe src="https://lichess.org/embed/uWllJ95r?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Brian Willcock v Andrew Robinson

<iframe src="https://lichess.org/embed/oUOhVso7?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

---
title: 2019 The Gap v Bullwinkle
date: 2019-06-28
authors: tmorris
---

The annual The Gap versus Bullwinkle tournament night was held this evening.

### Results

| # |   | The Gap                | SCORE  |   | Bullwinkle             | SCORE  |
|:-:|:-:|:----------------------:|:------:|:-:|:----------------------:|:------:|
| 1 | B | Joshua Morris          | ½      | W | Jacob Edwards          | ½      |
| 2 | W | Tony Howes             | 0      | B | Nik Stawski            | 1      |
| 3 | B | Tony Morris            | 0      | W | Michael D'Arcy         | 1      |
| 4 | W | Phillip Kirkman        | ½      | B | Konrad Uebel           | ½      |
| 5 | B | Taylor Reilly          | 0      | W | Halim Nataprawira      | 1      |
| 6 | W | Cameron Reilly         | 1      | B | Lavinia Poruschi       | 0      |
| 7 | B | George Flitcroft-Smith | ½      | W | Jared Mares            | ½      |
| 8 | W | Warren Ward            | 1      | B | Mark Pendrith          | 0      |
|   |   |                        | **3½** |   |                        | **4½** |

----

##### Jacob Edwards v Joshua Morris

<iframe src="https://lichess.org/embed/h5ObO6HA?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Tony Howes v Nik Stawski

<iframe src="https://lichess.org/embed/61GQrp5v?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Michael D'Arcy v Tony Morris

<iframe src="https://lichess.org/embed/ROnzRHz6?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Phillip Kirkman v Konrad Uebel

<iframe src="https://lichess.org/embed/Yzepc0MM?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Cameron Reilly v Lavinia Poruschi

<iframe src="https://lichess.org/embed/T8ISNopS?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Halim Nataprawira v Taylor Reilly

<iframe src="https://lichess.org/embed/TxIGLBW7?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Warren Ward v Mark Pendrith

<iframe src="https://lichess.org/embed/YABfxo9y?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

![](/images/20190628/IMG_20190628_204725.jpg)

----

![](/images/20190628/IMG_20190628_204723.jpg)

----

![](/images/20190628/IMG_20190628_204819.jpg)

----

![](/images/20190628/IMG_20190628_211240.jpg)

----

![](/images/20190628/DSCN0381.jpg)

----

![](/images/20190628/DSCN0386.jpg)

----

![](/images/20190628/DSCN0391.jpg)

----

![](/images/20190628/DSCN0393.jpg)

----

![](/images/20190628/DSCN0382.jpg)

----

![](/images/20190628/DSCN0385.jpg)

----

![](/images/20190628/DSCN0387.jpg)

----

![](/images/20190628/DSCN0392.jpg)

---
title: 2019 Stuart Wilson (05 July)
date: 2019-07-05
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Tony Morris            | 1     | George Flitcroft-Smith | 0     |
| Rex Scarf              | 0     | John Nothdurft         | 1     |
| Mark Pendrith          | 1     | Brian Willcock         | 0     |

----

##### Tony Morris v George Flitcroft-Smith

<iframe src="https://lichess.org/embed/9a3kbhwt?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Rex Scarf v John Nothdurft

<iframe src="https://lichess.org/embed/Hn8g9cxE?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Mark Pendrith v Brian Willcock

<iframe src="https://lichess.org/embed/51TFY1EI?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

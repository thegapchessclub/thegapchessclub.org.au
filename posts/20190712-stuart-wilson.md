---
title: 2019 Stuart Wilson (12 July)
date: 2019-07-12
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Rex Scarf              | 1     | Mark Pendrith          | 0     |
| Brian Willcock         | 0     | John Nothdurft         | 1     |

----

##### Rex Scarf v Mark Pendrith

<iframe src="https://lichess.org/embed/rNeyy6Oi?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

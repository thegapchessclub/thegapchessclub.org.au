---
title: 2019 Stuart Wilson (19 July)
date: 2019-07-19
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Tony Morris            | ½     | Rex Scarf              | ½     |
| Andrew Robinson        | ½     | Warren Ward            | ½     |
| John Nothdurft         | 0     | Mark Pendrith          | 1     |
| Brian Willcock         | 1     | George Flitcroft-Smith | 0     |

----

##### Tony Morris v Rex Scarf

<iframe src="https://lichess.org/embed/Y7YoUgOk?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Andrew Robinson v Warren Ward

<iframe src="https://lichess.org/embed/E9OaB8aw?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### John Nothdurft v Mark Pendrith

<iframe src="https://lichess.org/embed/44uO3cas?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

---
title: 2019 Stuart Wilson (26 July)
date: 2019-07-26
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Tony Morris            | 1     | Brian Willcock         | 0     |
| Andrew Robinson        | ½     | Mark Pendrith          | ½     |
| John Nothdurft         | ½     | Warren Ward            | ½     |
| George Flitcroft-Smith | 1     | Rex Scarf              | 0     |

----

##### Tony Morris v Brian Willcock

<iframe src="https://lichess.org/embed/Z0RcZmFw?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Andrew Robinson v Mark Pendrith

<iframe src="https://lichess.org/embed/ZJA05BTG?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

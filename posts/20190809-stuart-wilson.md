---
title: 2019 Stuart Wilson (09 August)
date: 2019-08-09
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| George Flitcroft-Smith | 0     | Andrew Robinson        | 1     |
| Warren Ward            | 0     | Tony Morris            | 1     |
| Brian Willcock         | 1     | Rex Scarf              | 0     |

----

### Current Standings

| Rank | Name                   | Points | Played |
| :--: | ---------------------- | :----: | :----: |
| 1    | Tony Morris            | 4.5    | 6      |
|      |                        |        |        |
| 2    | John Nothdurft         | 3.5    | 6      |
|      |                        |        |        |
| 3    | George Flitcroft-Smith | 3.0    | 6      |
|      |                        |        |        |
| 4    | Andrew Robinson        | 3.5    | 5      |
|      |                        |        |        |
| 5    | Rex Scarf              | 2.5    | 7      |
|      |                        |        |        |
| 6    | Mark Pendrith          | 2.5    | 6      |
|      |                        |        |        |
| 7    | Warren Ward            | 2.0    | 5      |
|      |                        |        |        |
| 8    | Brian Willcock         | 2.5    | 7      |
|      |                        |        |        |

----

##### George Flitcroft-Smith v Andrew Robinson

<iframe src="https://lichess.org/embed/ueC2Tv8i?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Warren Ward v Tony Morris

<iframe src="https://lichess.org/embed/2sMUJSBH?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

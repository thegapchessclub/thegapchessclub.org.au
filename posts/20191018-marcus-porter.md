---
title: 2019 Marcus Porter Memorial
date: 2019-10-18
authors: tmorris
---

The Marcus Porter Memorial chess tournament will be held over two consecutive Friday evenings at Marist College Ashgrove on **25 October 2019** and **01 November 2019**

This tournament is a Chess Association of Queensland approved event.

----

### Details

<span style="font-size:x-large">Anyone welcome!</span>

* Eight round Swiss tournament
* **Time Control:** 10 minutes plus 2 seconds per move (Fischer)
* **Registration:** before 7:00 pm on each night
* The draw for Round One and Five will be conducted at 7:15 pm
* Estimated Round Times each night:
  * 7:20pm
  * 7:55pm
  * 8:30pm
  * 9:05pm
* **First Prize:** Personal trophy, and name inscribed on the perpetual trophy
* **Special Grob Prize!** Awarded to the player with the highest score playing the Grob opening
* **Entry fee too be paid on the night:**
  * $10 seniors
  * $5 juniors
* **For further details:**
  * [shane_mather@sunsuper.com.au](mailto:shane_mather@sunsuper.com.au) **OR** [contact@thegapchessclub.org.au](mailto:contact@thegapchessclub.org.au)
* Please confirm attendance to Shane by email or text message on `0437737537`

### Venue

<span style="font-size:x-large">The Draney Theatre, Marist College Ashgrove, Fraser Road, Ashgrove</span>

* [Google maps](https://goo.gl/maps/zGX5kf7xuCiKwZZF7)
* [openstreetmap](https://osm.org/go/ueD1wTiEh--?m=)
* [what3words award.scores.manual](https://map.what3words.com/award.scores.manual)

<div>
  <img src="/images/draney-theatre-marist-college.jpg" width="75%" alt="The Draney Theatre, Marist College Ashgrove, Fraser Road, Ashgrove"/>
</div>

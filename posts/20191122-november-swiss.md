---
title: November Swiss 2019
date: 2019-11-22
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Phil Eichinski         | 0     | Warren Ward            | 1     |
| Andrew Robinson        | 1     | Mark Pendrith          | 0     |
| Tony Morris            | 1     | Rex Scarf              | 0     |
| Joshua Morris          | 1     | Brian Willcock         | 0     |

----

##### Tony Morris v Rex Scarf

<iframe src="https://lichess.org/embed/9c4QN3Yd?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Andrew Robinson v Mark Pendrith

<iframe src="https://lichess.org/embed/faQvz2vA?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Phil Eichinski v Warren Ward

<iframe src="https://lichess.org/embed/6IGKgqSz?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Joshua Morris v Brian Willcock

<iframe src="https://lichess.org/embed/0yqLnPEY?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

---
title: November Swiss 2019
date: 2019-11-29
authors: tmorris
---

### Results

| WHITE                  | SCORE | BLACK                  | SCORE |
|:----------------------:|:-----:|:----------------------:|:-----:|
| Warren Ward            | 0     | Andrew Robinson        | 1     |
| Phil Eichinski         | 0     | Tony Morris            | 1     |
| Mark Pendrith          | 1     | Joshua Morris          | 0     |
| Jackie Cheng           | 1     | Brian Willcock         | 0     |

----

##### Tony Morris v Phil Eichinski

<iframe src="https://lichess.org/embed/QPucxi40?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Warren Ward v Andrew Robinson

<iframe src="https://lichess.org/embed/2INQDYsI?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

----

##### Mark Pendrith v Joshua Morris

<iframe src="https://lichess.org/embed/GP5gPnHs?theme=auto&amp;bg=auto" width=600 height=397 frameborder=0></iframe>

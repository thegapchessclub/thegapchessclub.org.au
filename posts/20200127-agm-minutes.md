---
title: 2019 Club AGM Minutes
date: 2020-01-27
authors: tmorris
---

* Date: 20200122
* Location: The Gap Tavern, Queensland

### Attended in person

* Tony Morris
* Rex Scarf
* Tony Howes
* Andrew Robinson
* Martin Laizans
* Amanda Ward (non-member)
* Mark Pendrith
* Phil Eichinski

----

### Attended via proxy

* Phillip Kirkman
* Edward Tan
* Warren Ward

----

### Previous minutes

* The minutes of the 2018 AGM were unanimously accepted.

### Matters arising from 2018 AGM minutes

* Recent print-outs of club documents have not been easily legible
  * Andrew R to get a new, laser printer
* New processes for handling casual visitors adopted during 2019 included stopping tournament play when visitors came
  * This has been successful and will be continued
* Changing starting time to 1930 UTC+10
  * All approved of this change, except Martin

### Management Committee Reports
* The President's Report was not presented formally by Andrew Robinson.
  * Instead, this was covered as a discussion of the current state of the club and the matters that arose are set out below in General Business below.
* The Treasurer's Report was presented by Tony Morris
  * The club's income exceeded its expenditure slightly for the year ending 30 Sep 2019.
  * Generally, the club's finances are healthy and there is no need to alter the current club fee structure.

### Election of office bearers for 2020
* All positions were declared vacant and the following office bearers were elected.
  * President: Andrew Robinson
  * Vice-President: Vacant
  * Secretary: Tony Morris
  * Treasurer: Tony Morris
  * Tournament Director/Arbiter: Andrew Robinson
  * Publicity Officer: Tony Howes
  * Equipment Officer: Tony Morris
  * CAQ Delegate: Andrew Robinson
* Candidates for all positions were elected unanimously and unopposed.

### General Business

* Alter the front page of the website when the club closed
  * Some visitors have arrived to the club when it is closed
  * Tony Morris will add a message to the website and is happy to be reminded if it is not there when it needs to be 
  * The website already has a schedule on it the front page, so it only needs a notice when an unanticipated closure is occurring
* Coffee cupboard
  * Warren Ward usually helps with this, but is unlikely to attend the first half of the year
  * Tony Morris is happy to set it up
  * Request help from those around to put it away at the end of the night
* Second set of keys to the club room
  * This hasn't happened yet.
* Martin proposed creating an internal Facebook group
  * Rex seconded the proposal
  * Motion passed, with Tony Morris and Mark Pendrith abstaining
  * Amanda Ward will create the Facebook group for the club
* Phil proposed that we find new ways to get more club members
  * Mark proposed clarifying that our relaxed tournament scheduling which allows deferral of games (not allowed at many other chess clubs) be better advertised
  * Mark proposed finding a different venue, and suggested a bar or cafe
  * Mark will investigate possibilities for a new venue and communicate it back to the club's management committee
* Martin proposes changing the club name
  * Martin suggested some people may think the club is restricted to the suburb of The Gap for its membership
  * There was no support for changing the name, and objections were offered to alternative proposals such as "Western Suburbs Chess Club"
  * Mark proposed altering the website to include a list of suburbs in our region (which may help with visibility of the website to search engines)
  * The suggestion to amend the website was agreed to
* Andrew proposes one (or more) nights per year to host a club night at a different venue e.g. The Gap Tavern
  * Mark will investigate a venue that is OK with this idea
  * Suggestions included pubs and coffee shops
  * It was proposed that these nights be non-tournament nights
  * Could be a Friday night or some other nominated time
  * This proposal was unanimously endorsed
* CAQ has a 3 year club membership option
  * This is now included on the 2020 TGCC membership application form
* Meeting closes 2020 (UTC+10)
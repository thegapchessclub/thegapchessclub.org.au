{ mkDerivation, aeson, base, containers, filepath, hakyll, pandoc
, scientific, stdenv, text, unordered-containers, zlib
}:
mkDerivation {
  pname = "thegapchessclub-org-au";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base containers filepath hakyll pandoc scientific text
    unordered-containers zlib
  ];
  license = stdenv.lib.licenses.bsd3;
}
